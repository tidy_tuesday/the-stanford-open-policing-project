# Tidy Tuesday - Week #12
### The Stanford Open Policing Project


See https://github.com/rfordatascience/tidytuesday for more information.



## Code Snippet
```r
library(tidyverse)
library(lubridate)
library(ghibli)

penn <- read_rds("../data/tr137st9964_pa_philadelphia_2019_02_25.rds")

penn <- penn %>% mutate(subject_race = str_to_title(subject_race)) 

penn_by_year_race <- penn %>%
  group_by(year = year(date), subject_race) %>%
  mutate(n = n()) 


penn_by_year_race %>%
  ungroup() %>%
  mutate(subject_race = fct_relevel(subject_race, "Black","White","Hispanic","Asian/Pacific Islander","Other/Unknown")) %>%
  ggplot(aes(year, n, col = subject_race)) +
  geom_line(size = 2, show.legend = F) +
  geom_point(shape = 21, size = 3, col = my_bkgd, aes(fill = subject_race), show.legend = F) + 
  facet_wrap(~ subject_race) +
  scale_color_manual(values = ghibli_palettes$PonyoDark) +
  scale_fill_manual(values = ghibli_palettes$PonyoDark) +
  scale_y_continuous(labels = scales::comma_format()) +
  labs(x = NULL, y = "Number of Stops",
       title = "Number of Police Stops in Philadelphia by Year and Race",
       subtitle = "Jan 2014 to Apr 2018 (Note that 2018 is incomplete)",
       caption = "Source: The Stanford Open Policing Project | By @DaveBloom11") +
  theme(panel.border = element_rect(color = "grey80", fill = NA))


```
![](yr_plot_all.png)

```r
penn_race_mult <- penn_by_year_race %>%
  filter(!is.na(subject_race),
         subject_race == "Black" || subject_race == "White") %>%
  select(year, subject_race, n) %>% 
  unique() %>%
  spread(subject_race, n) %>%
  mutate(mult = Black / White) 

penn_race_mult %>%
  ggplot(aes(year, mult, fill = as.factor(year))) +
  geom_col() +
  geom_text(aes(label = paste0(round(mult,1),"x")), 
            family = my_font, size = rel(8), color = my_bkgd, vjust = 1.5) +
  scale_y_continuous(labels = function(x) paste0(x,"x")) +
  scale_fill_manual(values = ghibli_palettes$PonyoDark) +
  labs(x = NULL, y = "",
       title = expression(atop("If you are black, you are now"~bolditalic('4 times')~"more likely to be stopped by the","Philadelphia police than if you are white, despite decline in total stops")),
       subtitle = "Ratio of Black Subjects to White Subjects, Jan 2014 to Apr 2018",
       caption = "Source: The Stanford Open Policing Project | By @DaveBloom11") +
  theme(legend.position = "none",
        plot.title = element_text(hjust = 0.5),
        plot.subtitle = element_text(hjust = 0.5, color = ghibli_palettes$PonyoDark[7]))

```
![](b2w_ratio.png)

```r
penn %>%
  mutate(subject_race = fct_relevel(subject_race, 
                                    "Black",
                                    "White",
                                    "Hispanic",
                                    "Asian/Pacific Islander",
                                    "Other/Unknown")) %>%
  ggplot(aes(lng,lat,col=subject_race)) +
  geom_point(shape=".", show.legend = F) +
  coord_quickmap() +
  annotate("text", x = -74.88, y = 39.955, label = "Black", 
           col = scales::viridis_pal()(5)[1], family = my_font, hjust = 1, size = 6) +
  annotate("text", x = -74.88, y = 39.93, label = "White", 
           col = scales::viridis_pal()(5)[2], family = my_font, hjust = 1, size = 6) +
  annotate("text", x = -74.88, y = 39.905, label = "Hispanic", 
           col = scales::viridis_pal()(5)[3], family = my_font, hjust = 1, size = 6) +
  annotate("text", x = -74.88, y = 39.88, label = "Asian/Pacific Islander", 
           col = scales::viridis_pal()(5)[4], family = my_font, hjust = 1, size = 6) +
  annotate("text", x = -74.88, y = 39.855, label = "Other/Unknown", 
           col = scales::viridis_pal()(5)[5], family = my_font, hjust = 1, size = 6) +
  scale_color_viridis_d() +
  labs(x=NULL,y="",
       title = "Where are police stopping people\nin Philadelphia?",
       subtitle = "2014-2018",
       caption = "Source: The Stanford Open Policing Project | By @DaveBloom11") +
  theme(axis.text = element_blank())
```
![](map.png)